<?php

use App\Context\ServerRequestContext;
use App\Core\ControllerClassMappingResolver;
use App\Core\JsonSerializer;
use App\Request\HttpServerRequestCallable;
use App\Route\RouteMappingMethodResolver;
use Illuminate\Container\Container;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Level;
use Monolog\Logger;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Yaml\Yaml;

require_once __DIR__ . '/vendor/autoload.php';

$value = Yaml::parseFile( __DIR__ .'/config/app.yaml');
// initialize logger
$logger = new Logger($value['log']['channel']);
$logger->setTimezone(new DateTimeZone('Asia/Shanghai'));
$logger->pushHandler(new RotatingFileHandler($value['log']['path']), 1 << 20, Level::Info);

$container = Container::getInstance();
// load logger into ApplicationContainer
$container->singleton(Logger::class, function() use ($logger) {
    return $logger;
});

$container->singleton(JsonSerializer::class, function() {
    return new JsonSerializer(new JsonEncoder(), new ObjectNormalizer());
});

$mappingMethodResolver = new RouteMappingMethodResolver();
$defaultCtrClass = new ReflectionClass(App\Controller\NoInstantaneousController::class);
$controllerPath = str_replace(['App\\'], ['/'], $defaultCtrClass->getNamespaceName());

$directoryIterator = new RecursiveDirectoryIterator("./src" . $controllerPath);
try {
    $c = new ControllerClassMappingResolver($container,
        App\Controller\NoInstantaneousController::class,
        __DIR__ . '/src',
        $mappingMethodResolver);
    $c->resolve();
    dump(array_keys($mappingMethodResolver->getRouteMethodMappingView()));
} catch (ReflectionException $e) {
    dump($e->getTraceAsString());
} catch (NotFoundExceptionInterface| ContainerExceptionInterface $e) {
    dump($e->getTraceAsString());
}


$http = new React\Http\HttpServer(
    new React\Http\Middleware\StreamingRequestMiddleware(),
    new React\Http\Middleware\LimitConcurrentRequestsMiddleware(100), // 100 concurrent buffering handlers
    new React\Http\Middleware\RequestBodyBufferMiddleware(2 * 1024 * 1024), // 2 MiB per request
    new React\Http\Middleware\RequestBodyParserMiddleware(),
    new HttpServerRequestCallable($container, $mappingMethodResolver)
);

$socket = new React\Socket\SocketServer('0.0.0.0:8081');
$http->listen($socket);

$http->on('error', function (Exception $e) {
    echo 'Error: ' . $e->getMessage() . PHP_EOL;
    if ($e->getPrevious() !== null) {
        echo  ' Previous: ' . $e->getPrevious()->getMessage() . ", file:"
        . $e->getPrevious()->getFile()
        . " # line : " . $e->getPrevious()->getLine() . PHP_EOL;
    }
});

 echo 'Listening on ' . str_replace('tcp:', 'http:', $socket->getAddress()) . PHP_EOL;