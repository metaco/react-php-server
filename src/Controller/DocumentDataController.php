<?php

namespace App\Controller;

use App\Annotation\PathVariableType;
use App\Annotation\RequestMapping;
use App\Annotation\RequestPathVariable;
use App\Annotation\RestController;
use App\Entity\Document;
use App\Request\HttpRequestMethod;
use App\Service\DocumentService;

#[RestController(name: 'document')]
#[RequestMapping(path: '/document', method: HttpRequestMethod::ANY)]
class DocumentDataController
{
    private DocumentService $documentService;

    /**
     * @param DocumentService $documentService
     */
    public function __construct(DocumentService $documentService)
    {
        $this->documentService = $documentService;
    }

    #[RequestMapping(path: '/{id}', method: HttpRequestMethod::GET)]
    public function index(string $id) : string
    {
        return "resource";
    }

    #[RequestMapping(path: '/', method: HttpRequestMethod::POST)]
    public function create(Document $document): int
    {
        return 0;
    }

    #[RequestMapping(path: '/{id}', method: HttpRequestMethod::PUT)]
    #[RequestPathVariable(variableName: 'id', variableType: PathVariableType::INT)]
    public function update(Document $document, int $id): int
    {
        return 0;
    }

    #[RequestMapping(path: '/all', method: HttpRequestMethod::GET)]
    public function getAll(): int
    {
        return mb_strlen($this->documentService->getAllDocument());
    }
}