<?php

namespace App\Controller;

use App\Annotation\RequestMapping;
use App\Annotation\RestController;
use App\Entity\StorageMedia;
use App\Request\HttpRequestMethod;

 #[RestController(name: 'storageMedia')]
 #[RequestMapping(path: '/storage', method: HttpRequestMethod::ANY)]
class StorageMediaController
{
    #[RequestMapping(path: '/{id}', method: HttpRequestMethod::GET)]
    public function index(string $id) : string
    {
        return "resource";
    }

    #[RequestMapping(path: '/', method: HttpRequestMethod::POST)]
    public function create(StorageMedia $document): int
    {
        return 0;
    }

     #[RequestMapping(path: '/all', method: HttpRequestMethod::GET)]
     public function getAll(): int
     {
         return 1;
     }
}