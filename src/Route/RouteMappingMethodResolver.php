<?php

namespace App\Route;

use App\Request\HttpRequestMethod;
use App\Request\RequestControllerMappingView;
use App\Request\RequestParameterMappingView;
use ReflectionClass;

class RouteMappingMethodResolver
{
    private array $routeMapping = [];

    private array $routeMethodMappingView = [];

    public function resolveAttributeRoute(ReflectionClass $class, string $classAttributeName): void
    {
        $classResourcePath = "";
        $attribute = $class->getAttributes();
        foreach ($attribute as $itemAttribute) {
            $argument = $itemAttribute->getArguments();
            if (count($argument) > 0 && array_key_exists('path', $argument)) {
                $classResourcePath = $argument['path'];
            }
        }

        foreach ($class->getMethods() as $method) {
            $reflectionAttributes = $method->getAttributes();
            foreach ($reflectionAttributes as $attribute) {
                $attributeArg = $attribute->getArguments();

                if (count($attributeArg) < 1) {
                    continue;
                }

                if (array_key_exists('variableName', $attributeArg)
                    && array_key_exists('variableType', $attributeArg)) {
                    //
                }

                if (!array_key_exists('path', $attributeArg)) {
                    continue;
                }
                if (!array_key_exists('method', $attributeArg)) {
                    continue;
                }

                $mappingRoute = sprintf("%s:%s%s",
                    $attributeArg['method'] instanceof HttpRequestMethod ? $attributeArg['method']->value : "",
                    mb_strlen($classResourcePath) > 0 ? $classResourcePath : "",
                    $attributeArg['path'],
                );

                if (!in_array($mappingRoute, $this->routeMapping)) {
                    $this->routeMapping[] = $mappingRoute;
                    $reflectionParameters = $method->getParameters();

                    $controllerMappingView = new RequestControllerMappingView($classAttributeName, $method->getName());
                    $requestParameterMappingViewSet = [
                        'controller' => $controllerMappingView,
                        'parameter' => []
                    ];
                    foreach ($reflectionParameters as $parameter) {
                        $parameterMappingView = new RequestParameterMappingView($parameter->getName(), $parameter->getType());
                        $requestParameterMappingViewSet['parameter'][] = $parameterMappingView;
                    }
                    $this->routeMethodMappingView[$mappingRoute] = $requestParameterMappingViewSet;
                }
            }

        }
    }

    public function getRouteMethodMappingView(): array
    {
        return $this->routeMethodMappingView;
    }
}