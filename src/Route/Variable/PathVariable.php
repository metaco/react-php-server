<?php

namespace App\Route\Variable;

final class PathVariable
{
    private string $path;

    private string $variableName;

    private string $variableType;

    /**
     * @param string $path
     * @param string $variableName
     * @param string $variableType
     */
    public function __construct(string $path, string $variableName, string $variableType)
    {
        $this->path = $path;
        $this->variableName = $variableName;
        $this->variableType = $variableType;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getVariableName(): string
    {
        return $this->variableName;
    }

    /**
     * @return string
     */
    public function getVariableType(): string
    {
        return $this->variableType;
    }
}