<?php

namespace App\Annotation;

enum PathVariableType : string
{
    case INT = 'int';
    case STRING = 'string';
    case UNKNOWN = 'unknown';

    public function getTypeValue(): string
    {
        return $this->value;
    }

    public static function match(string $typeName) : PathVariableType
    {
        foreach (self::cases() as $type) {
            if ($type->name == $typeName) {
                return $type;
            }
        }
        return PathVariableType::UNKNOWN;
    }
}
