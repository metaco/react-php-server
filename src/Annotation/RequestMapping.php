<?php

namespace App\Annotation;

use App\Request\HttpRequestMethod;
use Attribute;

#[Attribute]
class RequestMapping
{
    private string $path;

    private HttpRequestMethod $method;

    /**
     * @param string $path
     * @param HttpRequestMethod $method
     */
    public function __construct(string $path, HttpRequestMethod $method)
    {
        $this->path = $path;
        $this->method = $method;
    }

    /**
     * @return HttpRequestMethod
     */
    public function getMethod(): HttpRequestMethod
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }
}