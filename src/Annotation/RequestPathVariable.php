<?php

namespace App\Annotation;

use Attribute;

#[Attribute]
class RequestPathVariable
{
    private string $variableName;

    private PathVariableType $variableType;

    /**
     * @param string $variableName
     * @param PathVariableType $variableType
     */
    public function __construct(string $variableName, PathVariableType $variableType)
    {
        $this->variableName = $variableName;
        $this->variableType = $variableType;
    }

    /**
     * @return string
     */
    public function getVariableName(): string
    {
        return $this->variableName;
    }

    /**
     * @return PathVariableType
     */
    public function getVariableType(): PathVariableType
    {
        return $this->variableType;
    }


}