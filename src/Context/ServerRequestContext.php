<?php

namespace App\Context;

use App\Request\RequestAttribute;
use Psr\Http\Message\ServerRequestInterface;

final class ServerRequestContext
{
    public function handle(ServerRequestInterface $request): RequestAttribute
    {
        $requestAttribute = new RequestAttribute();
        $requestAttribute->setHeaders($request->getHeaders());
        $requestAttribute->setHost($request->getUri()->getHost());
        $requestAttribute->setMethod($request->getMethod());
        $requestAttribute->setOriginalPath($request->getUri()->getPath());
        $requestAttribute->setPort($request->getUri()->getPort());
        $requestAttribute->setQueryParameters($request->getQueryParams());
        $requestAttribute->setRequestContentCopy($request->getBody());
        $requestAttribute->setRequestData($request->getParsedBody());

        return $requestAttribute;
    }
}