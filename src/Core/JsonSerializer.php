<?php

namespace App\Core;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class JsonSerializer
{
    private Serializer $serializer;

    public function __construct(JsonEncoder $jsonEncoder, ObjectNormalizer $normalizer)
    {
        $this->serializer = new Serializer([ $normalizer], [$jsonEncoder]);
    }

    public function jsonEncode($object) : string
    {
        return $this->serializer->serialize($object, 'json');
    }

    public function jsonSpecific($object) : string
    {
        return $this->serializer->serialize($object, JsonEncoder::FORMAT, [
            AbstractObjectNormalizer::SKIP_NULL_VALUES       => true,
            AbstractObjectNormalizer::PRESERVE_EMPTY_OBJECTS => true,
        ]);
    }

    public function jsonDecode(string $data, string $clazzName)
    {
        return $this->serializer->deserialize($data, $clazzName, 'json');
    }
}