<?php

namespace App\Core;

use App\Route\RouteMappingMethodResolver;
use Illuminate\Container\Container;
use Monolog\Logger;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use RecursiveDirectoryIterator;
use ReflectionClass;
use ReflectionException;

class ControllerClassMappingResolver
{
    private Container $applicationContextContainer;

    private ReflectionClass $defaultControllerReflectionClass;
    private RouteMappingMethodResolver $mappingMethodResolver;
    private string $controllerClassPath;

    /**
     * @param Container $applicationContextContainer
     * @param object|string $defaultClassFullName
     * @param string $absoluteControllerClassPath
     * @param RouteMappingMethodResolver $mappingMethodResolver
     * @throws ReflectionException
     */
    public function __construct(Container $applicationContextContainer,
                                object|string $defaultClassFullName,
                                string $absoluteControllerClassPath,
                                RouteMappingMethodResolver $mappingMethodResolver)
    {
        $this->applicationContextContainer = $applicationContextContainer;
        $this->defaultControllerReflectionClass = new ReflectionClass($defaultClassFullName);
        $this->controllerClassPath = $absoluteControllerClassPath;
        $this->mappingMethodResolver = $mappingMethodResolver;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function resolve(): void
    {
        $controllerPath = str_replace(['App\\'], ['/'], $this->defaultControllerReflectionClass->getNamespaceName());
        $directoryIterator = new RecursiveDirectoryIterator($this->controllerClassPath . $controllerPath);

        /** @var Logger $logger */
        $logger = $this->applicationContextContainer->get(Logger::class);
        /** @var JsonSerializer $jsonSerializer */
        // $jsonSerializer = $this->applicationContextContainer->get(JsonSerializer::class);

        foreach ($directoryIterator as $file) {
            if ($file->isDir()) {
                continue ;
            }
            $classFullName = str_replace(['.php'], [''], $file->getFilename());
            $controllerClassRf = null;
            $controllerClassAttributeName = "";
            try {
                $containsNamespaceClassName = sprintf("%s\\%s",
                    $this->defaultControllerReflectionClass->getNamespaceName() ,
                    $classFullName
                );
                $controllerClassRf = new ReflectionClass($containsNamespaceClassName);
                $logger->info("loading class :" . $containsNamespaceClassName);

                $reflectionAttributes = $controllerClassRf->getAttributes();
                foreach ($reflectionAttributes as $attribute) {
                    if ('App\Annotation\RestController' == $attribute->getName()
                        && array_key_exists('name', $attribute->getArguments())) {
                        $controllerClassAttributeName = $attribute->getArguments()['name'];
                            $this->applicationContextContainer
                                ->bind($controllerClassAttributeName, $containsNamespaceClassName);
                        }
                }
            } catch (ReflectionException $e) {
                $logger->error("ReflectionException :". $e->getMessage());
            }
            if ($controllerClassRf === null) {
                continue ;
            }

            $this->mappingMethodResolver->resolveAttributeRoute($controllerClassRf, $controllerClassAttributeName);
        }
    }
}