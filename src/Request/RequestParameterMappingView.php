<?php

namespace App\Request;

class RequestParameterMappingView
{
    private string $parameterName;

    private string $fullParameterTypeClassName;

    /**
     * @param string $parameterName
     * @param string $fullParameterTypeClassName
     */
    public function __construct(string $parameterName, string $fullParameterTypeClassName)
    {
        $this->parameterName = $parameterName;
        $this->fullParameterTypeClassName = $fullParameterTypeClassName;
    }

    /**
     * @return string
     */
    public function getParameterName(): string
    {
        return $this->parameterName;
    }

    /**
     * @return string
     */
    public function getFullParameterTypeClassName(): string
    {
        return $this->fullParameterTypeClassName;
    }
}