<?php

namespace App\Request;

enum HttpRequestContentType : string
{
    case CONTENT_TYPE_JSON_UTF8 = "Content-Type:application/json;utf-8";
    case CONTENT_TYPE_JSON = "Content-Type:application/json";
    case CONTENT_TYPE_WWW_URLENCODED = "Content-Type:application/x-www-url-form-encoded";
}