<?php

namespace App\Request;

enum HttpRequestMethod : string
{
    case GET = 'GET';
    case POST = 'POST';
    case PUT = 'PUT';
    case DELETE = 'DELETE';
    case PATCH = 'PATCH';
    case HEAD = 'HEAD';
    case ANY = 'ANY';

    public function getValue(): string
    {
         return $this->value;
    }
}
