<?php

namespace App\Request;

use App\Context\ServerRequestContext;
use App\Route\RouteMappingMethodResolver;
use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use Psr\Http\Message\ServerRequestInterface;
use React\Http\Message\Response;

class HttpServerRequestCallable
{
    private Container $container;

    private RouteMappingMethodResolver $methodResolver;

    /**
     * @param Container $container

     * @param RouteMappingMethodResolver $methodResolver
     */
    public function __construct(Container $container,
                                RouteMappingMethodResolver $methodResolver)
    {
        $this->container = $container;
        $this->methodResolver = $methodResolver;
    }


    public function __invoke(ServerRequestInterface $request) : Response
    {
        $serverRequestContext = new ServerRequestContext();

        $requestAttribute = $serverRequestContext->handle($request);

        $routeMethodMappingView = $this->methodResolver->getRouteMethodMappingView();
        $mappingRoute = array_keys($routeMethodMappingView);
        $requestMethodPath = sprintf("%s:%s", $requestAttribute->getMethod(), $requestAttribute->getOriginalPath());

        if (!in_array($requestMethodPath, $mappingRoute)) {
            return Response::plaintext("invalid request path");
        }

        $mappingViewItem = $routeMethodMappingView[$requestMethodPath];
        if (!array_key_exists('controller', $mappingViewItem)) {
            return Response::plaintext("invalid configure for controller");
        }

        try {
            /** @var RequestControllerMappingView $requestCtrMappingView */
            $requestCtrMappingView = $mappingViewItem['controller'];
            $controller = $this->container->make($requestCtrMappingView->getClassAttributeName());

            $parameters = [];

            $resp = $this->container->call([$controller, $requestCtrMappingView->getMethodName()], $parameters);
            return Response::plaintext(
                "Hello World!\n"
                ." result : {$resp} \n"
                . 'memory_limit : ' .ini_get("memory_limit") . "\n"
                . 'post_max_size : ' .ini_get("post_max_size") . "\n"
                . 'file_uploads : ' .ini_get("file_uploads") . "\n"
                . 'requestUri Host:' . $request->getUri()->getHost() . "\n"
                . 'requestUri Path:' . $request->getUri()->getPath() . "\n"
                . 'requestUri Query:' . $request->getUri()->getQuery() . "\n"
                . 'requestUri Schema:' . $request->getUri()->getScheme() . "\n"
                . 'requestUri Port:' . $request->getUri()->getPort() . "\n"
                . 'requestUri Auth:' . $request->getUri()->getAuthority() . "\n"
                . 'requestUri UserInfo:' . $request->getUri()->getUserInfo() . "\n"
                . 'requestMethod:' . $request->getMethod() . "\n"
                . 'getProtocolVersion-------------------->:' . $request->getProtocolVersion() . "\n"
                . 'Attributes:' . json_encode($request->getAttributes()) . "\n"
                . 'Headers:' . json_encode($request->getHeaders()) . "\n"
                . 'ParseBody:' . json_encode($request->getParsedBody()) . "\n"
                . 'Body:' . $request->getBody()->getContents() . "\n"
                . 'BodySize:' . $request->getBody()->getSize() . "\n"
                . 'MetaData:' . json_encode($request->getBody()->getMetadata()) . "\n"
                . 'BodyReadable:' . json_encode($request->getBody()->isReadable()) . "\n"
                . 'ServerParams:' . json_encode($request->getServerParams()) . "\n"
                . 'QueryParams:' . json_encode($request->getQueryParams()) . "\n"
            );
        } catch (BindingResolutionException $e) {
            return Response::plaintext($e->getMessage());
        }
    }
}