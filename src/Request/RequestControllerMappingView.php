<?php

namespace App\Request;

class RequestControllerMappingView
{
    private string $classAttributeName;

    private string $methodName;

    /**
     * @param string $classAttributeName
     * @param string $methodName
     */
    public function __construct(string $classAttributeName, string $methodName)
    {
        $this->classAttributeName = $classAttributeName;
        $this->methodName = $methodName;
    }

    /**
     * @return string
     */
    public function getClassAttributeName(): string
    {
        return $this->classAttributeName;
    }

    /**
     * @return string
     */
    public function getMethodName(): string
    {
        return $this->methodName;
    }




}