<?php

namespace App\Request;

class RequestAttribute
{
    private string $host;

    private string $path;

    private string $originalPath;

    private int $port;

    private string $method;

    private string $requestContentCopy;

    private array|null|object $requestData;

    private array $headers;

    private array $queryParameters;

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost(string $host): void
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return int
     */
    public function getPort(): int
    {
        return $this->port;
    }

    /**
     * @param int $port
     */
    public function setPort(int $port): void
    {
        $this->port = $port;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod(string $method): void
    {
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function getRequestContentCopy(): string
    {
        return $this->requestContentCopy;
    }

    /**
     * @param string $requestContentCopy
     */
    public function setRequestContentCopy(string $requestContentCopy): void
    {
        $this->requestContentCopy = $requestContentCopy;
    }

    /**
     * @return array|object|null
     */
    public function getRequestData(): object|array|null
    {
        return $this->requestData;
    }

    /**
     * @param array|object|null $requestData
     */
    public function setRequestData(object|array|null $requestData): void
    {
        $this->requestData = $requestData;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers): void
    {
        $this->headers = $headers;
    }

    /**
     * @return array
     */
    public function getQueryParameters(): array
    {
        return $this->queryParameters;
    }

    /**
     * @param array $queryParameters
     */
    public function setQueryParameters(array $queryParameters): void
    {
        $this->queryParameters = $queryParameters;
    }

    /**
     * @return string
     */
    public function getOriginalPath(): string
    {
        return $this->originalPath;
    }

    /**
     * @param string $originalPath
     */
    public function setOriginalPath(string $originalPath): void
    {
        $this->originalPath = $originalPath;
    }


}